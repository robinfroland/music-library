# Music Library
A simple Spring Boot CRUD application using an existing database to query information about music. The frontend is written in Thymeleaf and shows 5 random songs, artists and genres. It also allows users to search for specific songs.

## Demo
Live demo is deployed to Heroku:
https://spring-music-library.herokuapp.com/

## API Endpoints

### `GET` Get all customers
Returns information about all the customers.

    /api/customers/

### `POST` Create new customer
Accepts a customer object in request body and inserts values into database.

    /api/customers/

### `PUT` Update existing customer
Accepts a customer object with updated values in request body and updates database accordingly.

    /api/customers/{id}

### `GET` Countries by customers
Returns all countries and the number of customers in each country, ordered descending.

    /api/customers/country/top

### `GET` Highest spenders
Returns the customers with the highest total invoice, ordered descending.

    /api/customers/spending/top

### `GET` Customers' popular genres
Returns the most popular genre(s) for a given customer and the number of times the genre corresponds to tracks from their invoice.

    /api/customers/{id}/popular/genre