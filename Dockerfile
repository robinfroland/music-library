FROM openjdk:14
ADD target/music-library-0.0.1-SNAPSHOT.jar musiclibrary.jar
ENTRYPOINT [ "java", "-jar", "musiclibrary.jar" ]