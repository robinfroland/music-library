package com.experis.musiclibrary.models.endpoint_queries;

public class CustomersInCountry {

    private final String country;
    private final int numberOfCustomers;

    public CustomersInCountry(String country, int numberOfCustomers) {
        this.numberOfCustomers = numberOfCustomers;
        this.country = country;
    }

    public int getNumberOfCustomers() {
        return numberOfCustomers;
    }

    public String getCountry() {
        return country;
    }
}
