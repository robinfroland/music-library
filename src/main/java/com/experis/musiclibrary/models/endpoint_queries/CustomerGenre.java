package com.experis.musiclibrary.models.endpoint_queries;

public class CustomerGenre {

    private String genreId;
    private String name;
    private int nOccurrences;

    public CustomerGenre(String genreId, String name, int nOccurrences) {
        this.genreId = genreId;
        this.name = name;
        this.nOccurrences = nOccurrences;
    }

    public String getGenreId() {
        return genreId;
    }

    public String getName() {
        return name;
    }

    public int getNumOccurrences() {
        return nOccurrences;
    }
}
