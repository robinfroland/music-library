package com.experis.musiclibrary.models.endpoint_queries;
public class CustomerSpending {

    private String customerId;
    private String customerFirstName;
    private String customerLastName;
    private String spending;

    public CustomerSpending(String customerId, String customerFirstName, String customerLastName, String spending) {
        this.customerId = customerId;
        this.customerFirstName = customerFirstName;
        this.customerLastName = customerLastName;
        this.spending =  spending;
    }

    public String getCustomerId() {
        return customerId;
    }

    public String getCustomerFirstName() {
        return customerFirstName;
    }

    public String getCustomerLastName() {
        return customerLastName;
    }

    public String getSpending() {
        return spending;
    }

}
