package com.experis.musiclibrary.controllers;

import com.experis.musiclibrary.data_access.ArtistDao;
import com.experis.musiclibrary.data_access.GenreDao;
import com.experis.musiclibrary.data_access.TrackDao;
import com.experis.musiclibrary.data_access.SearchDao;
import com.experis.musiclibrary.models.endpoint_queries.TrackInformation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import javax.validation.constraints.*;
import java.util.List;

@Controller
public class HomeController {
    SearchDao searchDao = new SearchDao();
    ArtistDao artist = new ArtistDao();
    TrackDao track = new TrackDao();
    GenreDao genre = new GenreDao();

    @GetMapping("/")
    public String home(Model model) {
        int nResults = 5;
        model.addAttribute("randomArtists", artist.getRandom(nResults));
        model.addAttribute("randomTracks", track.getRandom(nResults));
        model.addAttribute("randomGenres", genre.getRandom(nResults));
        return "home";
    }

    @GetMapping("/search")
    public String search(@NotNull @NotEmpty @RequestParam(required = false) String query, Model model ) {
        if (query == null || query.isEmpty()) {
            return "redirect:/";
        }
        List<TrackInformation> results = searchDao.getTrackInformation(query);
        model.addAttribute("query", query);
        model.addAttribute("results", results);
        return "search_results";
    }
}
