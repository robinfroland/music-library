package com.experis.musiclibrary.controllers;

import com.experis.musiclibrary.data_access.CustomerDao;
import com.experis.musiclibrary.data_access.endpoints.customer.CustomerGenreDao;
import com.experis.musiclibrary.data_access.endpoints.customer.CustomersCountryDao;
import com.experis.musiclibrary.data_access.endpoints.customer.CustomerSpendingDao;
import com.experis.musiclibrary.models.Customer;
import com.experis.musiclibrary.models.endpoint_queries.CustomerGenre;
import com.experis.musiclibrary.models.endpoint_queries.CustomerSpending;
import com.experis.musiclibrary.models.endpoint_queries.CustomersInCountry;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class CustomerController {
    CustomerDao customerDoa = new CustomerDao();
    CustomersCountryDao customersCountryDao = new CustomersCountryDao();
    CustomerSpendingDao customerSpendingDao = new CustomerSpendingDao();
    CustomerGenreDao customerGenreDao = new CustomerGenreDao();

    @GetMapping("api/customers")
    public List<Customer> getAllCustomers() {
        return customerDoa.getAllCustomers();
    }

    @PostMapping("/api/customers/")
    public ResponseEntity<String> createCustomer(@RequestBody Customer customer) {
        boolean success = customerDoa.addCustomer(customer);
        return success ? new ResponseEntity<>(HttpStatus.CREATED) : new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @PutMapping("api/customers/{id}")
    public ResponseEntity<String> updateCustomer(@PathVariable String id, @RequestBody Customer customer) {
        boolean success = customerDoa.updateCustomer(customer, id);
        return success ? new ResponseEntity<>(HttpStatus.OK) : new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @GetMapping("api/customers/country/top")
    public List<CustomersInCountry> getCountriesByNumCustomers() {
        return customersCountryDao.getCountriesByNumCustomers();
    }

    @GetMapping("api/customers/spending/top")
    public List<CustomerSpending> getCustomersBySpending() {
        return customerSpendingDao.getCustomersBySpending();
    }

    @GetMapping("api/customers/{id}/popular/genre")
    public List<CustomerGenre> getCustomersPopularGenres(@PathVariable String id) {
        return customerGenreDao.getCustomersPopularGenres(id);
    }

}
