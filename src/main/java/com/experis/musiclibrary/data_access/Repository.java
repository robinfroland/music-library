package com.experis.musiclibrary.data_access;

import java.util.List;

interface Repository<T> {
    List<T> getRandom(int nResults);
}
