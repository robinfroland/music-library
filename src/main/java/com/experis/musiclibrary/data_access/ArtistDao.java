package com.experis.musiclibrary.data_access;

import com.experis.musiclibrary.data_access.Repository;
import com.experis.musiclibrary.models.Artist;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ArtistDao implements Repository<Artist> {

    private static final String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private static Connection connection = null;

    @Override
    public List<Artist> getRandom(int nResults) {
        List<Artist> randomArtists = new ArrayList<>();

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prepStatement = connection.prepareStatement(
                    "SELECT ArtistId, Name FROM artist ORDER BY RANDOM() LIMIT 5");
            ResultSet resultSet = prepStatement.executeQuery();

            while (resultSet.next()) {
                randomArtists.add(new Artist(
                        resultSet.getString("ArtistId"),
                        resultSet.getString("name")));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        return randomArtists;
    }
}
