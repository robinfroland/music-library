package com.experis.musiclibrary.data_access;

import com.experis.musiclibrary.models.Customer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CustomerDao {

    private static final String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private static Connection connection = null;

    public List<Customer> getAllCustomers() {
        List<Customer> customers = new ArrayList<>();

        try {
            connection = DriverManager.getConnection(URL);

            String selectQuery = "SELECT * FROM customer";
            PreparedStatement prepStatement = connection.prepareStatement(selectQuery);
            ResultSet resultSet = prepStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(new Customer(
                        resultSet.getString("customerId"),
                        resultSet.getString("firstName"),
                        resultSet.getString("lastName"),
                        resultSet.getString("country"),
                        resultSet.getString("postalCode"),
                        resultSet.getString("phone"),
                        resultSet.getString("Email")
                ));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        return customers;
    }

    public boolean addCustomer(Customer newCustomer) {
        boolean success = false;

        try {
            connection = DriverManager.getConnection(URL);

            String insertQuery = "INSERT INTO customer ";
            insertQuery += "(FirstName, LastName, Country, PostalCode, Phone, Email, SupportRepId) ";
            insertQuery += "VALUES (?,?,?,?,?,?,?)";

            PreparedStatement prepStatement = connection.prepareStatement(insertQuery);
            prepStatement.setString(1, newCustomer.getFirstName());
            prepStatement.setString(2, newCustomer.getLastName());
            prepStatement.setString(3, newCustomer.getCountry());
            prepStatement.setString(4, newCustomer.getPostalCode());
            prepStatement.setString(5, newCustomer.getPhoneNumber());
            prepStatement.setString(6, "1");

            success = prepStatement.executeUpdate() != 0;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        return success;
    }

    public boolean updateCustomer(Customer updatedCustomer, String customerId) {
        boolean success = false;

        try {
            connection = DriverManager.getConnection(URL);

            String updateQuery = "UPDATE customer ";
            updateQuery += "SET FirstName = ?, LastName = ?, Country = ?, PostalCode = ?, Phone = ? ";
            updateQuery += "WHERE CustomerId = ?";

            PreparedStatement prepStatement = connection.prepareStatement(updateQuery);
            prepStatement.setString(1, updatedCustomer.getFirstName());
            prepStatement.setString(2, updatedCustomer.getLastName());
            prepStatement.setString(3, updatedCustomer.getCountry());
            prepStatement.setString(4, updatedCustomer.getPostalCode());
            prepStatement.setString(5, updatedCustomer.getPhoneNumber());
            prepStatement.setString(6, updatedCustomer.getCustomerId());

            success = prepStatement.executeUpdate() != 0;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        return success;
    }

}
