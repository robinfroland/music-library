package com.experis.musiclibrary.data_access;

import com.experis.musiclibrary.models.Genre;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GenreDao implements Repository<Genre> {

    private static final String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private static Connection connection = null;

    public List<Genre> getRandom(int nResults) {
        List<Genre> randomGenres = new ArrayList<>();

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prepStatement = connection.prepareStatement(
                    "SELECT GenreId, Name FROM genre ORDER BY RANDOM() LIMIT 5");
            ResultSet resultSet = prepStatement.executeQuery();

            while (resultSet.next()) {
                randomGenres.add(new Genre(
                        resultSet.getString("GenreId"),
                        resultSet.getString("name")));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        return randomGenres;
    }
}
