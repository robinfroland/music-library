package com.experis.musiclibrary.data_access;

import com.experis.musiclibrary.models.endpoint_queries.TrackInformation;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SearchDao {

    private static final String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private static Connection connection = null;


    public List<TrackInformation> getTrackInformation(String searchInput){
        List<TrackInformation> tracks = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);

            String selectQuery = "SELECT track.Name as TrackName, artist.Name as ArtistName, album.Title as AlbumTitle, genre.Name as GenreName ";
            selectQuery += "FROM track NATURAL JOIN album ";
            selectQuery += "INNER JOIN artist on artist.ArtistId = album.ArtistID ";
            selectQuery += "INNER JOIN genre on genre.GenreId = track.GenreId ";
            selectQuery += "WHERE LOWER(track.Name) LIKE ('%" + searchInput + "%')";

            PreparedStatement prepStatement = connection.prepareStatement(selectQuery);
            ResultSet resultSet = prepStatement.executeQuery();

            while (resultSet.next()) {
                tracks.add(new TrackInformation(
                                resultSet.getString("TrackName"),
                                resultSet.getString("ArtistName"),
                                resultSet.getString("AlbumTitle"),
                                resultSet.getString("GenreName")
                        ));
            }
        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
        finally {
            try {
                connection.close();
            }
            catch (Exception e){
                System.out.println(e.getMessage());
            }
        }
        return tracks;
    }
}