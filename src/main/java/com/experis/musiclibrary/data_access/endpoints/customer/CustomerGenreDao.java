package com.experis.musiclibrary.data_access.endpoints.customer;

import com.experis.musiclibrary.models.endpoint_queries.CustomerGenre;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CustomerGenreDao {

    private static final String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private static Connection connection = null;

    public List<CustomerGenre> getCustomersPopularGenres(String id){
        List<CustomerGenre> popularGenres = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);

            String tableJoin = "NATURAL JOIN invoice NATURAL JOIN invoiceline ";
            tableJoin += "INNER JOIN genre on genre.GenreId = track.GenreId ";
            tableJoin += "INNER JOIN track on invoiceline.TrackId = track.TrackId ";
            tableJoin += "WHERE CustomerId = ? GROUP BY genre.GenreId) ";

            String selectQuery = "SELECT GenreName, GenreCount FROM ";
            // Get count grouped by genres (subquery 1)
            selectQuery += "(SELECT genre.Name as GenreName, COUNT(*) as GenreCount FROM customer ";
            selectQuery += tableJoin;
            // Set WHERE condition equals to the max number of times a genre occurred in users invoice
            selectQuery += "WHERE GenreCount = (SELECT MAX(GenreCount) FROM ";
            // Return name and count of genres satisfying above condition (subquery 2)
            selectQuery += "(SELECT genre.Name as GenreName, COUNT(*) as GenreCount FROM customer ";
            selectQuery += tableJoin + ")";

            PreparedStatement prepStatement = connection.prepareStatement(selectQuery);
            prepStatement.setString(1, id);
            prepStatement.setString(2, id);
            ResultSet resultSet = prepStatement.executeQuery();

            while (resultSet.next()) {
                popularGenres.add(new CustomerGenre(
                        id,
                        resultSet.getString("GenreName"),
                        resultSet.getInt("GenreCount")));
            }
        }
        catch (SQLException e){
            System.out.println(e.getMessage());
        }
        finally {
            try {
                connection.close();
            }
            catch (Exception e){
                System.out.println(e.getMessage());
            }
        }

        return popularGenres;
    }
}
