package com.experis.musiclibrary.data_access.endpoints.customer;

import com.experis.musiclibrary.models.endpoint_queries.CustomerSpending;

import java.sql.*;
import java.util.ArrayList;

public class CustomerSpendingDao {

    private static final String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private static Connection connection = null;

    public ArrayList<CustomerSpending> getCustomersBySpending(){
        ArrayList<CustomerSpending> customersBySpending = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);

            String selectQuery = "SELECT CustomerId, FirstName, LastName, SUM(Total) as Spending";
            selectQuery += " FROM customer NATURAL JOIN invoice GROUP BY CustomerId";
            selectQuery += " ORDER BY Spending DESC";

            PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customersBySpending.add(
                        new CustomerSpending(
                                resultSet.getString("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Spending")
                        ));
            }
        }
        catch (SQLException e){
            System.out.println(e.getMessage());
        }
        finally {
            try {
                connection.close();
            }
            catch (Exception e){
                System.out.println(e.getMessage());
            }
        }

        return customersBySpending;
    }
}
