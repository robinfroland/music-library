package com.experis.musiclibrary.data_access.endpoints.customer;

import com.experis.musiclibrary.models.endpoint_queries.CustomersInCountry;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CustomersCountryDao {

    private static final String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private static Connection connection = null;


    public List<CustomersInCountry> getCountriesByNumCustomers(){
        List<CustomersInCountry> customersByNumCustomers = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);

            String selectQuery = "SELECT Country, Count(CustomerId) as numOfCustomers";
            selectQuery += " FROM customer GROUP BY Country";
            selectQuery += " ORDER BY Count(CustomerId) DESC";

            PreparedStatement prepStatement = connection.prepareStatement(selectQuery);
            ResultSet resultSet = prepStatement.executeQuery();

            while (resultSet.next()) {
                customersByNumCustomers.add(
                        new CustomersInCountry(
                                resultSet.getString("Country"),
                                resultSet.getInt("numOfCustomers")

                        ));
            }
        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
        finally {
            try {
                connection.close();
            }
            catch (Exception e){
                System.out.println(e.getMessage());
            }
        }
        return customersByNumCustomers;
    }
}
