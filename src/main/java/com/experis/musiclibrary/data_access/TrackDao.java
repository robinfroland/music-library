package com.experis.musiclibrary.data_access;

import com.experis.musiclibrary.models.Track;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TrackDao implements Repository<Track> {

    private static final String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private static Connection connection = null;

    public List<Track> getRandom(int nResults) {
        List<Track> randomTracks = new ArrayList<>();

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prepStatement = connection.prepareStatement(
                    "SELECT TrackId, Name FROM track ORDER BY RANDOM() LIMIT 5");
            ResultSet resultSet = prepStatement.executeQuery();

            while (resultSet.next()) {
                 randomTracks.add(new Track(
                         resultSet.getString("trackId"),
                         resultSet.getString("name")));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        return randomTracks;
    }

}
